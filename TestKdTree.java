public class TestKdTree {
    public static void main(String[] args) {
        KdTree a = new KdTree();
        a.addPoint(3, 5, 8);
        a.addPoint(1, 7, 8);
        a.addPoint(8, 9, 17);

        a.search(8, 9);

        a.Imprimir();

        System.out.println("la cantidad de personas dentro del rango es: " + a.contarRango(1, 6, 3, 7, 4));
        System.out.println("la cantidad de personas dentro del rango es: " + a.contarRango(5, 6, 3, 7, 9));

        System.out.print("\n");
        System.out.println(a.factor(8, 9));
    }
}
