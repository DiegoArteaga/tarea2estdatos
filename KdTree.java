/*
    -Diego Arteaga
    -Rodrigo Jara
    -Sebastian Jouannet
*/
public class KdTree {
    Punto root;

    public KdTree() {
        root = null;
    }

    // CONSTRUCTOR
    private class Punto {
        int x = 0, y = 0;
        float factor = 0f;
        Punto left = null;
        Punto right = null;

        public Punto(int x, int y, float factor) {
            this.x = x;
            this.y = y;
            this.factor = factor;
            this.left = null;
            this.right = null;
        }
    }

    // ADD
    public void addPoint(int x, int y, float factor) {
        root = addP(x, y, factor, 0, root);
        System.out.println("punto agregado");
    }

    private Punto addP(int x, int y, float factor, int chek, Punto root) {
        if (root == null) {
            root = new Punto(x, y, factor);
        } else if (chek == 0) {
            if (x < root.x) {
                root.left = addP(x, y, factor, (chek + 1) % 2, root.left);
            } else {
                root.right = addP(x, y, factor, (chek + 1) % 2, root.right);
            }
        } else if (y < root.y) {
            root.left = addP(x, y, factor, (chek + 1) % 2, root.left);
        } else {
            root.right = addP(x, y, factor, (chek + 1) % 2, root.right);
        }
        return root;
    }

    // BUSCAR
    public void search(int x, int y) {
        search(root, x, y);
    }

    private void search(Punto root, int x, int y) {
        if (root != null) {
            search(root.left, x, y);
            if (root.x == x && root.y == y) {
                System.out.println("Punto encontrado");
            }
            search(root.right, x, y);
        }
    }

    // FACTOR
    public float factor(int edad, int peso) {
        float factor = searchFactor(edad, peso, root, 0);
        return factor;
    }

    public float searchFactor(int edad, int peso, Punto root, int chek) {
        if (root == null) {
            return 0;
        }
        float factor = 0;
        if (edad == root.x && peso == root.y) {
            factor = root.factor;
        } else if (chek == 0) {
            if (edad < root.x) {
                factor = searchFactor(edad, peso, root.left, (chek + 1) % 2);
            } else {
                factor = searchFactor(edad, peso, root.right, (chek + 1) % 2);
            }
        } else {
            if (peso < root.y) {
                factor = searchFactor(edad, peso, root.left, (chek + 1) % 2);
            } else {
                factor = searchFactor(edad, peso, root.right, (chek + 1) % 2);
            }
        }
        return factor;
    }

    // CONTAR EN UN RANGO
    public int contarRango(int minEdad, int maxEdad, int minPeso, int maxPeso, float minFactor) {
        int cantPer = Rango(root, minEdad, maxEdad, minPeso, maxPeso, minFactor, 0);
        return cantPer;
    }

    private int Rango(KdTree.Punto root, int minEdad, int maxEdad, int minPeso, int maxPeso, float minFactor,
            int check) {
        int personas = 0;
        /*
         * en bucle buscar todos los nodos que cumplan con
         * - x >= minEdad && <= maxEdad
         * - y >= minPeso && <= maxPeso
         * - factor >= minFactor
         * validar si un subarbol esta dentro de los parametros sino se descarta
         * Una ves realizadas las validaciones si se cumple con todo aumentar en 1
         * variable "personas"
         */
        if (root == null) {
            return 0;
        }
        if ((root.x >= minEdad && root.x <= maxEdad) && (root.y >= minPeso && root.y <= maxPeso)
                && (root.factor >= minFactor)) {
            personas++;
        }
        if (check == 0) {
            if (root.x >= minEdad) {
                personas += Rango(root.left, minEdad, maxEdad, minPeso, maxPeso, minFactor, (check + 1) % 2);
            }
            if (root.x <= maxEdad) {
                personas += Rango(root.right, minEdad, maxEdad, minPeso, maxPeso, minFactor, (check + 1) % 2);
            }
        } else if (check == 1) {
            if (root.y >= minPeso) {
                personas += Rango(root.left, minEdad, maxEdad, minPeso, maxPeso, minFactor, (check + 1) % 2);
            }
            if (root.y <= maxPeso) {
                personas += Rango(root.right, minEdad, maxEdad, minPeso, maxPeso, minFactor, (check + 1) % 2);
            }
        }
        return personas;
    }

    // IMPRIMIR
    public void Imprimir() {
        ImprimirKd(root, " ");
    }

    private void ImprimirKd(Punto n, String tab) {
        if (n != null) {
            System.out.println(tab + "->" + n.x + "," + n.y);
            ImprimirKd(n.left, tab + "  |");
            ImprimirKd(n.right, tab + "  |");
        }
    }
}